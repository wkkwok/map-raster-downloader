module.exports = (api) => {
	api.cache(true);

	return {
		presets: [
			[
				'@babel/env',
				{
					targets: {
						node: 'current',
					},
				},
			],
			'@babel/preset-typescript',
		],
		plugins: [
			['@babel/plugin-proposal-decorators', { legacy: true }],
			'@babel/plugin-proposal-class-properties',
		],
		env: {
			build: {
				ignore: ['**/*.spec.ts'],
			},
		},
		ignore: ['node_modules'],
	};
};
