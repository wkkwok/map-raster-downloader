## Map Raster Downloader

#### Installation

```bash
git clone https://gitlab.com/wkkwok/map-raster-downloader.git

npm install
# OR
yarn install

# You copy the following config.json and make changes needed
./dist/cli.js -c <path to config.json>
```

#### Config

```json
{
	// Template url for the raster server
	"url": [
		"http://a.tile.openstreetmap.org/{z}/{x}/{y}.png",
		"http://b.tile.openstreetmap.org/{z}/{x}/{y}.png",
		"http://c.tile.openstreetmap.org/{z}/{x}/{y}.png"
	],
	// Bounding box to download in WGS84
	"bbox": [113.839645, 22.161335, 114.502945, 22.553782],
	// Zoom levels to download
	"zoomLevel": [6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
	// No of workers for parallel download, you may test if different number to get best result. One could estimate the speed from the progress bar image/s value
	"noOfWorkers": 8,
	// Currently only osmdroid cache db is supported
	"storage": "OSMDROID",
	// Where the output file place
	"outDir": "./",
	// The name of output file, no extension is needed. Extension will be set according to storage engine
	"outFilename": "cache",
	// Dataset ID
	"provider": "osm"
}
```
