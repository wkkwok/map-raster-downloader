import {
	IsString,
	IsNumber,
	IsOptional,
	IsEnum,
	ArrayMinSize,
	ArrayMaxSize,
	Max,
	Min,
	IsUrl,
	ValidateIf,
	ValidateNested,
} from 'class-validator';
import { BBox } from './types';

export enum StorageEngine {
	OSMDROID = 'OSMDROID',
	ZIP = 'ZIP',
	FOLDER = 'FOLDER',
}

export enum FolderStructure {
	ZXY = 'ZXY',
	ZYX = 'ZYX',
}

export enum URLScheme {
	XYZ = 'XYZ',
	BBOX = 'BBOX',
}

export class Task {
	constructor(rawConfig: any) {
		Object.assign(this, rawConfig);
	}

	@IsUrl({}, { each: true })
	@ArrayMinSize(1)
	url: string[] = [];

	@IsString()
	@IsOptional()
	description?: string;

	@IsEnum(URLScheme)
	@IsOptional()
	urlScheme: URLScheme = URLScheme.XYZ;

	headers: { [key: string]: string } = {};

	@IsNumber({}, { each: true })
	@ArrayMinSize(4)
	@ArrayMaxSize(4)
	bbox!: BBox;

	@IsNumber({}, { each: true })
	@Max(30, { each: true })
	@Min(0, { each: true })
	@ArrayMinSize(1)
	zoomLevel!: number[];

	@IsEnum(StorageEngine)
	storage: StorageEngine = StorageEngine.OSMDROID;

	@IsEnum(FolderStructure)
	@ValidateIf(
		(o) =>
			o.storage === StorageEngine.FOLDER ||
			o.storage === StorageEngine.ZIP,
	)
	folderStructure: FolderStructure = FolderStructure.ZXY;

	@IsString()
	@ValidateIf(
		(o) =>
			o.storage === StorageEngine.FOLDER ||
			o.storage === StorageEngine.ZIP,
	)
	extension = '.png';

	@IsString()
	@IsOptional()
	expiresIn?: string;

	@IsString()
	@IsOptional()
	outFilename = 'raster';

	@IsString()
	@IsOptional()
	provider = 'unknown';

	@IsNumber()
	@IsOptional()
	tileSize = 256;

	@IsString()
	@IsOptional()
	srs: 'WGS84' | '900913' = 'WGS84';
}

export class Config {
	constructor(rawConfig: any) {
		const { tasks, ...rest } = rawConfig;
		Object.assign(this, {
			...rest,
			tasks: tasks.map((task: any) => new Task(task)),
		});
	}
	@IsNumber()
	noOfWorkers = 1;

	@IsOptional()
	@IsString()
	proxyUri?: string;

	@IsString()
	@IsOptional()
	proxyAuthType?: 'basic' | 'ntlm';

	@IsString()
	@ValidateIf((o) => o.proxyAuthType === 'ntlm')
	ntlmDomain?: string;

	@IsString()
	@IsOptional()
	ntlmWorkstation?: string;

	@IsString()
	@IsOptional()
	userAgent = 'Rasterd';

	@IsString()
	@IsOptional()
	outDir = './';

	@ValidateNested({ each: true })
	tasks!: Task[];
}
