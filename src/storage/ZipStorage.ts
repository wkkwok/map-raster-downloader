import { Tile } from '../types';
import { Response } from 'node-fetch';
import { resolve } from 'path';
import { rmdir, WriteStream, createWriteStream } from 'fs';

import archiver from 'archiver';

import { Task } from '../config';
import { FolderStorage } from './FolderStorage';
import { promisify } from 'util';

const rmdirAsync = promisify(rmdir);
export class ZipStorage extends FolderStorage {
	zip!: WriteStream;

	async finalize() {
		const { outDir } = this.config;
		const { outFilename } = this.task;
		const zipPath = resolve(process.cwd(), outDir, `${outFilename}.zip`);
		const dataPath = resolve(process.cwd(), outDir, outFilename);

		const zip = createWriteStream(zipPath);

		const archive = archiver('zip');

		zip.on('close', function () {
			console.log(archive.pointer() + ' total bytes');
			console.log(
				'archiver has been finalized and the output file descriptor has closed.',
			);
		});
		zip.on('end', function () {
			console.log('Data has been drained');
		});

		archive.on('error', console.error);

		archive.pipe(zip);
		archive.directory(dataPath + '/', false);

		await archive.finalize();

		await rmdirAsync(dataPath, { recursive: true });
	}
}
