import { AbstractStorage } from './AbstractStorage';

import { Tile } from '../types';
import { Task } from '../config';
import { resolve } from 'path';
// import { existsSync } from 'fs';
import { Database } from 'sqlite3';
import parseDuration from 'parse-duration';
import Long from 'long';

import {
	OSMDROID_CACHE_TABLE,
	OSMDROID_CACHE_COLUMN_EXPIRES,
	OSMDROID_CACHE_COLUMN_EXPIRES_INDEX,
	OSMDROID_CACHE_COLUMN_PROVIDER,
	OSMDROID_CACHE_COLUMN_TILE,
	OSMDROID_CACHE_COLUMN_KEY,
} from '../constants';
import { Response } from 'node-fetch';
import { debuglog } from 'util';

const debug = debuglog('rasterd:osmdroid');

export class OsmdroidStorage extends AbstractStorage {
	db!: Database;

	runQuery(query: string, params?: any): Promise<any> {
		return new Promise((resolve, reject) => {
			this.db.run(query, params, function (err: any) {
				if (err) {
					reject(err);
				}
				resolve(this);
			});
		});
	}

	async initDatabaseFile() {
		const { outDir } = this.config;
		const { outFilename, extension } = this.task;
		const sqliteFile = resolve(
			process.cwd(),
			outDir,
			`${outFilename}${extension}`,
		);

		return new Promise((resolve, reject) => {
			this.db = new Database(sqliteFile, (err) => {
				if (err) {
					reject(err);
				} else {
					resolve();
				}
			});
		});
	}

	async init() {
		await this.initDatabaseFile();
		await this.runQuery(
			`CREATE TABLE IF NOT EXISTS ${OSMDROID_CACHE_TABLE} (${OSMDROID_CACHE_COLUMN_KEY} INTEGER, ${OSMDROID_CACHE_COLUMN_PROVIDER} TEXT, ${OSMDROID_CACHE_COLUMN_TILE} BLOB, ${OSMDROID_CACHE_COLUMN_EXPIRES} INTEGER, PRIMARY KEY (${OSMDROID_CACHE_COLUMN_KEY}, ${OSMDROID_CACHE_COLUMN_PROVIDER}));`,
		);
		await this.runQuery(
			`CREATE INDEX IF NOT EXISTS ${OSMDROID_CACHE_COLUMN_EXPIRES_INDEX} ON ${OSMDROID_CACHE_TABLE} (${OSMDROID_CACHE_COLUMN_EXPIRES});`,
		);
	}

	async save(tile: Tile, res: Response) {
		const { expiresIn, provider } = this.task;
		const { x, y, z } = tile;

		const key = Long.fromInt(z)
			.shiftLeft(Long.fromInt(z))
			.add(Long.fromInt(x))
			.shiftLeft(Long.fromInt(z))
			.add(Long.fromInt(y))
			.toString();

		debug(`Inserting tile of key: ${key}`);

		try {
			if (expiresIn) {
				const expires =
					new Date().getTime() +
					(parseDuration(expiresIn, 'ms') || 0);
				await this.runQuery(
					`INSERT INTO ${OSMDROID_CACHE_TABLE} (${OSMDROID_CACHE_COLUMN_KEY}, ${OSMDROID_CACHE_COLUMN_TILE}, ${OSMDROID_CACHE_COLUMN_EXPIRES}, ${OSMDROID_CACHE_COLUMN_PROVIDER}) VALUES ($key, $tile, $expires, $provider)`,
					{
						$key: key,
						$tile: await res.buffer(),
						$expires: expires,
						$provider: provider,
					},
				);
			} else {
				await this.runQuery(
					`INSERT INTO ${OSMDROID_CACHE_TABLE} (${OSMDROID_CACHE_COLUMN_KEY}, ${OSMDROID_CACHE_COLUMN_TILE}, ${OSMDROID_CACHE_COLUMN_PROVIDER}) VALUES ($key, $tile, $provider)`,
					{
						$key: key,
						$tile: await res.buffer(),
						$provider: provider,
					},
				);
			}
		} catch (err) {
			err;
		}
	}
}
