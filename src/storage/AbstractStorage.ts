import { Tile } from '../types';
import { Task, Config } from '../config';
import { Response } from 'node-fetch';

export abstract class AbstractStorage {
	constructor(public config: Config, public task: Task) {}

	abstract async init(): Promise<void>;

	abstract async save(tile: Tile, image: Response): Promise<void>;

	async finalize(): Promise<void> {}
}
