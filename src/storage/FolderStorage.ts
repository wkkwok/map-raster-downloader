import { AbstractStorage } from './AbstractStorage';
import { Tile } from '../types';
import { Response } from 'node-fetch';
import { Task, FolderStructure, Config } from '../config';
import mkdirp from 'mkdirp';
import { resolve } from 'path';
import { createWriteStream } from 'fs';

export class FolderStorage extends AbstractStorage {
	async init() {}

	async save(tile: Tile, res: Response) {
		const { outDir } = this.config;
		const { outFilename, folderStructure, extension } = this.task;
		const { z, x, y } = tile;
		const folder =
			folderStructure === FolderStructure.ZXY ? `${z}/${x}` : `${z}/${y}`;
		const storePath = resolve(process.cwd(), outDir, outFilename, folder);
		await mkdirp(storePath);
		const filePath = resolve(
			storePath,
			folderStructure === FolderStructure.ZXY
				? `${y}${extension}`
				: `${x}${extension}`,
		);
		const stream = createWriteStream(filePath);
		res.body.pipe(stream);
		await new Promise((resolve, reject) => {
			stream.on('finish', resolve);
			stream.on('error', reject);
		});
	}
}
