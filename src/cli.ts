#!/usr/bin/env node
import cluster from 'cluster';
import { Command } from 'commander';
import SphericalMercator from '@mapbox/sphericalmercator';
import { validateOrReject } from 'class-validator';
import { readFileSync } from 'fs';
import { resolve } from 'path';
import ProgressBar from 'progress';
import { prompt } from 'inquirer';

import { startDownloadWorker } from './worker';
import { Task, Config } from './config';
import { Tile } from './types';
import { writeIndex } from './writeIndex';

import packageJSON from '../package.json';
import { WorkerClient } from './WorkerClient';

function* createTileGenerator(task: Task) {
	const { tileSize, zoomLevel, bbox, srs } = task;
	const merc = new SphericalMercator({ size: tileSize });

	for (const z of zoomLevel) {
		const { maxX, maxY, minX, minY } = merc.xyz(bbox, z, false, srs);
		for (let x = minX; x < maxX + 1; x++) {
			for (let y = minY; y < maxY + 1; y++) {
				const tile: Tile = { x, y, z };
				yield tile;
			}
		}
	}
}

function getTileCount(task: Task) {
	const { tileSize, zoomLevel, bbox, srs } = task;
	const merc = new SphericalMercator({ size: tileSize });
	return zoomLevel.reduce((r, z) => {
		const { maxX, maxY, minX, minY } = merc.xyz(bbox, z, false, srs);
		return r + (maxX - minX + 1) * (maxY - minY + 1);
	}, 0);
}

async function runTask(workers: WorkerClient[], task: Task) {
	const { outFilename } = task;

	const tileGenerator = createTileGenerator(task);

	const tileCount = getTileCount(task);
	console.log('Tile count:', tileCount);

	const progress = new ProgressBar(
		`Downloading ${outFilename} [:bar] :rate/s :percent :etas`,
		{ total: tileCount, complete: '=', incomplete: ' ', width: 20 },
	);

	await Promise.all(workers.map((worker) => worker.initTask(task)));

	await Promise.all(
		workers.map((worker) => worker.downloadTiles(tileGenerator, progress)),
	);
	await workers[0].finalize();
}

async function main() {
	if (cluster.isMaster) {
		const program = new Command();
		program
			.version(packageJSON.version)
			.option('-d, --debug <scope>', 'Output extra debugging')
			.option(
				'-c, --config <config file>',
				'Select config file',
				'config.json',
			)
			.option('-a, --all', 'Run all tasks')
			.parse(process.argv);

		const configJSON = readFileSync(
			resolve(process.cwd(), program.config),
		).toString();
		const config: Config = new Config(JSON.parse(configJSON));

		await validateOrReject(config);

		console.table(config);
		console.log('Config file is valid, start initializing workers');

		const { noOfWorkers, tasks } = config;
		let selectedTasks = tasks;

		const workers: WorkerClient[] = [];

		if (!program.all) {
			const choices = tasks.map((task) => {
				const { outFilename, description } = task;
				return {
					name: `${outFilename}${
						description ? ` - ${description}` : ''
					}`,
					value: task,
				};
			});
			const answers = await prompt([
				{
					type: 'checkbox',
					name: 'tasks',
					message: 'Please select tasks to run',
					default: tasks,
					choices: choices,
				},
			]);
			selectedTasks = answers.tasks;
		}
		for (let i = 0; i < noOfWorkers; i++) {
			const socket = cluster.fork({
				NODE_DEBUG: program.debug,
			});
			const workerClient = new WorkerClient(config, socket);
			await workerClient.init();
			console.log(`Worker ${i} is ready for task`);
			workers.push(workerClient);
		}
		for (let i = 0, len = selectedTasks.length; i < len; i++) {
			try {
				const task = selectedTasks[i];
				console.log(`Starting ${i + 1} of ${len} tasks`);
				console.table({
					...task,
					url: task.url.join(', '),
					bbox: `[${task.bbox.join(', ')}]`,
					zoomLevel: task.zoomLevel.join(', '),
				});
				const startTime = new Date().getTime();
				await runTask(workers, task);
				await writeIndex(config, task, startTime);
			} catch (err) {
				console.error(err);
			}
		}

		workers.forEach((worker) => worker.kill());
	} else {
		await startDownloadWorker();
	}
}

main().catch(console.error);
