export interface Tile {
	x: number;
	y: number;
	z: number;
}

export type BBox = [number, number, number, number];
