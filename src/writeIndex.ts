import { promises } from 'fs';
import { Config, Task } from './config';
import { resolve } from 'path';

interface IndexEntry extends Task {
	updatedAt: number;
	timeSpent: number;
}

export async function writeIndex(
	config: Config,
	task: Task,
	startTime: number,
) {
	const { outDir } = config;
	const updatedAt = new Date().getTime();
	const indexPath = resolve(process.cwd(), outDir, 'index.json');
	let fileIndexJSON = '[]';
	try {
		fileIndexJSON = await promises.readFile(indexPath, {
			encoding: 'utf-8',
			flag: 'r+',
		});
	} catch (err) {
		console.log(err);
	}
	let fileIndex: IndexEntry[];
	try {
		fileIndex = JSON.parse(fileIndexJSON);
	} catch (err) {
		fileIndex = [];
	}
	const { outFilename } = task;
	const index = fileIndex.findIndex(
		({ outFilename: target }) => outFilename === target,
	);
	const entry = {
		...task,
		updatedAt,
		timeSpent: (updatedAt - startTime) / 1000 / 60,
	};
	if (index !== -1) {
		fileIndex[index] = entry;
	} else {
		fileIndex.push(entry);
	}

	await promises.writeFile(indexPath, JSON.stringify(fileIndex, null, '\t'));
}
