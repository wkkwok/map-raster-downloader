import { Worker } from 'cluster';
import { Config, Task } from './config';
import { Tile } from './types';
import { MESSAGE_TYPE } from './constants';
import { debuglog } from 'util';

const debug = debuglog('rasterd:cluster');

export class WorkerClient {
	constructor(private config: Config, private socket: Worker) {}

	init() {
		return new Promise((resolve, reject) => {
			this.socket.on('online', () => {
				this.socket.addListener(
					'message',
					(message: { type: MESSAGE_TYPE; errorMessage: string }) => {
						const { type, errorMessage } = message;
						debug('WORKER:', message);
						switch (type) {
							case MESSAGE_TYPE.INIT_WORKER_SUCCESS:
								resolve();
								break;
							case MESSAGE_TYPE.INIT_WORKER_FAILURE:
								reject(new Error(errorMessage));
								break;
						}
					},
				);
				this.socket.send({
					type: MESSAGE_TYPE.INIT_WORKER,
					config: this.config,
				});
			});
		});
	}

	initTask(task: Task) {
		const socket = this.socket;

		return new Promise((resolve, reject) => {
			function handleInitTask(message: { type: MESSAGE_TYPE }) {
				switch (message.type) {
					case MESSAGE_TYPE.INIT_TASK_SUCCESS:
						resolve();
						break;
					case MESSAGE_TYPE.INIT_TASK_FAILURE:
						reject();
						socket.removeListener('message', handleInitTask);
						break;
				}
			}
			this.socket.addListener('message', handleInitTask);
			this.socket.send({ type: MESSAGE_TYPE.INIT_TASK, task });
		});
	}

	async downloadTile(tile: Tile, failCount = 0) {
		const socket = this.socket;

		try {
			await new Promise<void>((resolve, reject) => {
				function handleDownload(message: { type: MESSAGE_TYPE }) {
					switch (message.type) {
						case MESSAGE_TYPE.DOWNLOAD_SUCCESS:
							resolve();
							break;
						case MESSAGE_TYPE.DOWNLOAD_FAILURE:
							reject();
							break;
					}
					socket.removeListener('message', handleDownload);
				}
				this.socket.addListener('message', handleDownload);
				this.socket.send({ type: MESSAGE_TYPE.DOWNLOAD, tile });
			});
		} catch {
			if (failCount < 10) await this.downloadTile(tile, failCount + 1);
		}
	}

	async downloadTiles(
		tileGenerator: Generator<Tile>,
		progress: ProgressBar,
	): Promise<void> {
		const { value: tile, done } = tileGenerator.next();
		if (done) {
			return;
		}
		await this.downloadTile(tile);
		progress.tick();
		return await this.downloadTiles(tileGenerator, progress);
	}

	async finalize() {
		const socket = this.socket;

		await new Promise<void>((resolve, reject) => {
			function handleFinalize(message: { type: MESSAGE_TYPE }) {
				switch (message.type) {
					case MESSAGE_TYPE.TASK_FINALIZE_SUCCESS:
						resolve();
						break;
					case MESSAGE_TYPE.TASK_FINALIZE_FAILURE:
						reject();
						break;
				}
				socket.removeListener('message', handleFinalize);
			}
			this.socket.addListener('message', handleFinalize);
			this.socket.send({ type: MESSAGE_TYPE.TASK_FINALIZE });
		});
	}

	kill() {
		this.socket.removeAllListeners();
		this.socket.kill();
	}
}
