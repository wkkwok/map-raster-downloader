import fetch from 'node-fetch';
import { debuglog } from 'util';

import SphericalMercator from '@mapbox/sphericalmercator';
import { Task, URLScheme, Config } from './config';
import { Tile } from './types';

let converter: SphericalMercator | undefined;

const debug = debuglog('rasterd:downloadImage');

export async function downloadImage(tile: Tile, task: Task, config: Config) {
	const { url: urlList, headers, urlScheme, tileSize } = task;
	const { x, y, z } = tile;
	const url = urlList[Math.floor(Math.random() * urlList.length)];
	let tileURL;

	switch (urlScheme) {
		case URLScheme.XYZ:
			tileURL = url
				.replace('{x}', `${x}`)
				.replace('{y}', `${y}`)
				.replace('{z}', `${z}`);
			break;
		case URLScheme.BBOX: {
			converter = converter
				? converter
				: new SphericalMercator({ size: tileSize });
			const [w, s, e, n] = converter.bbox(x, y, z);
			tileURL = url
				.replace('{n}', `${n}`)
				.replace('{s}', `${s}`)
				.replace('{e}', `${e}`)
				.replace('{w}', `${w}`)
				.replace('{scale}', `${91657550.5 / Math.pow(2, z - 1)}`);
			break;
		}
		default:
	}

	if (!tileURL) {
		throw new Error('Invalid URL Scheme');
	}
	debug(`Start downloading image from ${tileURL}`);

	return await fetch(tileURL, {
		headers: {
			'User-Agent': config.userAgent,
			...headers,
			// Referer: 'localhost:8080',
		},
	});
}
