import { downloadImage } from './downloadImage';
import { OsmdroidStorage } from './storage/OsmdroidStorage';
import { Task, StorageEngine, Config } from './config';
import { debuglog } from 'util';
import { globalize } from 'proxying-agent';
import { AbstractStorage } from './storage/AbstractStorage';
import { ZipStorage } from './storage/ZipStorage';
import { FolderStorage } from './storage/FolderStorage';
import { Tile } from './types';
import { MESSAGE_TYPE } from './constants';

const debug = debuglog('rasterd:worker');

export async function startDownloadWorker() {
	let storageEngine: AbstractStorage;
	let config: Config;
	let task: Task | null = null;

	process.on(
		'message',
		async (msg: {
			type: MESSAGE_TYPE;
			config: Config;
			task: Task;
			tile: Tile;
		}) => {
			debug('CLUSTER:', msg);
			if (msg.type === MESSAGE_TYPE.INIT_WORKER) {
				const { config: _config } = msg;
				config = _config;
				const {
					proxyUri,
					proxyAuthType,
					ntlmDomain,
					ntlmWorkstation,
				} = config;
				if (proxyUri) {
					globalize({
						proxy: proxyUri,
						authType: proxyAuthType,
						ntlm: {
							domain: ntlmDomain as string,
							workstation: ntlmWorkstation || '',
						},
					});
				}

				process.send?.({ type: MESSAGE_TYPE.INIT_WORKER_SUCCESS });
			}

			if (msg.type === MESSAGE_TYPE.INIT_TASK) {
				const { task: _task } = msg;
				task = _task;
				const { storage } = task;

				try {
					switch (storage) {
						case StorageEngine.OSMDROID:
							storageEngine = new OsmdroidStorage(config, task);
							break;
						case StorageEngine.ZIP:
							storageEngine = new ZipStorage(config, task);
							break;
						case StorageEngine.FOLDER:
							storageEngine = new FolderStorage(config, task);
							break;
					}
					await storageEngine.init();
				} catch (err) {
					debug(err);
					process.send?.({ type: MESSAGE_TYPE.INIT_TASK_FAILURE });
				}

				process.send?.({ type: MESSAGE_TYPE.INIT_TASK_SUCCESS });
			}

			if (msg.type === MESSAGE_TYPE.DOWNLOAD) {
				const { tile } = msg;
				if (!task) {
					process.send?.({
						type: MESSAGE_TYPE.DOWNLOAD_FAILURE,
						tile,
					});
					return;
				}

				const res = await downloadImage(tile, task, config);
				const { ok, status } = res;
				if (ok) {
					await storageEngine.save(tile, res);
					process.send?.({
						type: MESSAGE_TYPE.DOWNLOAD_SUCCESS,
						tile,
					});
				} else if (status >= 300 && status < 500) {
					debug(
						`Fail to download tile: x=${tile.x}, y=${tile.y}, z=${tile.z}, and status code is ${res.status}, skipped`,
					);
					process.send?.({
						type: MESSAGE_TYPE.DOWNLOAD_SUCCESS,
						tile,
					});
				} else {
					debug(
						`Fail to download tile: x=${tile.x}, y=${tile.y}, z=${tile.z}, [${status}] ${res.statusText}`,
					);
					process.send?.({
						type: MESSAGE_TYPE.DOWNLOAD_FAILURE,
						tile,
					});
				}
			}

			if (msg.type === MESSAGE_TYPE.TASK_FINALIZE) {
				try {
					await storageEngine.finalize();
					process.send?.({
						type: MESSAGE_TYPE.TASK_FINALIZE_SUCCESS,
					});
				} catch (err) {
					debug(
						`Fail to finalize task: ${
							err?.message ? err.message : 'UNKNOWN REASON'
						}`,
					);
					process.send?.({
						type: MESSAGE_TYPE.TASK_FINALIZE_FAILURE,
					});
				}
			}
		},
	);
}
